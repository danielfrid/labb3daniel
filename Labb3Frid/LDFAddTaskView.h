//
//  LDFAddTaskView.h
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDFAddTaskView : UIViewController
@property (nonatomic) NSMutableArray *tasks; 
@end
