//
//  LDFAddTaskView.m
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LDFAddTaskView.h"
#import "LDFTask.h"
#import "LDFTaskMan.h"

@interface LDFAddTaskView ()
@property (weak, nonatomic) IBOutlet UITextField *taskHeadingText;
@property (weak, nonatomic) IBOutlet UITextField *taskText;
@property (weak, nonatomic) IBOutlet UISlider *taskPrioritySlide;
@property (weak, nonatomic) IBOutlet UILabel *taskPrioTextLabel;
@property (nonatomic) LDFTask *task;
@property (nonatomic) int prioValue;

@end

@implementation LDFAddTaskView
- (IBAction)addNewTaskButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.task = [[LDFTask alloc] init];
    self.task.toDo = self.taskText.text;
    self.task.taskHead = self.taskHeadingText.text;
    self.task.prio = self.prioValue;
    [self.tasks addObject:self.task];
   
    //saving at each place changing taskarray to prevent dataloss in case of softwarecrash (God forbid).
    [self saveArray];
}

-(void)saveArray{
//    NSLog(@"int saveArray");
    
    NSData *encodedTasks = [NSKeyedArchiver archivedDataWithRootObject:self.tasks];
    [[NSUserDefaults standardUserDefaults] setObject:encodedTasks forKey:[NSString stringWithFormat:@"tasks"]];
}
- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)taskPriorityValue:(id)sender {
    self.prioValue = 0;
    self.taskPrioritySlide.minimumValue = 1;
    self.taskPrioritySlide.maximumValue = 4;
    self.prioValue = self.taskPrioritySlide.value;
    switch (self.prioValue) {
        case 1:
            self.taskPrioTextLabel.text = @"No worries";
            self.taskPrioTextLabel.textColor = [UIColor greenColor];
            break;
        case 2:
            self.taskPrioTextLabel.text = @"Don't forget";
            self.taskPrioTextLabel.textColor = [UIColor blueColor];
            break;
        case 3:
            self.taskPrioTextLabel.text = @"This IS important";
            self.taskPrioTextLabel.textColor = [UIColor orangeColor];
            break;
        case 4:
            self.taskPrioTextLabel.text = @"Do it FFS!!";
            self.taskPrioTextLabel.textColor = [UIColor redColor];
            break;
        default:
            self.taskPrioTextLabel.text = @"Set Priority";
            
            break;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
