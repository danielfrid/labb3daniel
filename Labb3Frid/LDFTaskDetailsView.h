//
//  LDFTaskDetailsView.h
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDFTaskDetailsView : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *taskHeader;
@property (weak, nonatomic) IBOutlet UITextView *taskInfo;
@property (weak, nonatomic) IBOutlet UITextField *taskPrio;

@property (nonatomic) NSString *info;
@property (nonatomic) NSString *head;
@property (nonatomic) int prio;


@end
