//
//  LDFTask.h
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDFTask : NSObject <NSCoding>
@property (nonatomic) NSString *toDo;
@property (nonatomic) NSString *taskHead;
@property (nonatomic) int prio;

@end
