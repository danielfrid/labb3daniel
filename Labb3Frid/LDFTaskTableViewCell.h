//
//  LDFTaskTableViewCell.h
//  Labb3Frid
//
//  Created by IT-Högskolan on 03/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDFTaskTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *header;
@property (weak, nonatomic) IBOutlet UITextField *taskInfo;
@property (weak, nonatomic) IBOutlet UIImageView *taskPrio;

@end
