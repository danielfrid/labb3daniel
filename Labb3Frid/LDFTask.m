//
//  LDFTask.m
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LDFTask.h"
@interface LDFTask()
@end

@implementation LDFTask

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if(!self)
    {
        return nil;
    }
    self.toDo = [decoder decodeObjectForKey:@"toDo"];
    self.taskHead = [decoder decodeObjectForKey:@"taskHead"];
    self.prio = [decoder decodeIntegerForKey:@"prio"];
    
    return self;
}




- (void)encodeWithCoder:(NSCoder *)encoder
{
    
    [encoder encodeObject:self.toDo forKey:@"toDo"];
    [encoder encodeObject:self.taskHead forKey:@"taskHead"];
    [encoder encodeInteger:self.prio forKey:@"prio"];
    
    
}

@end
