//
//  LDFTaskMan.m
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LDFTaskMan.h"
#import "LDFTask.h"
#import "LDFTaskDetailsView.h"
#import "LDFAddTaskView.h"


@interface LDFTaskMan ()
@property (nonatomic) NSMutableArray *tasks;
@property (nonatomic) int taskPriority;
@property (nonatomic) int currentRow;
@end

@implementation LDFTaskMan

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadArray];
    if(!self.tasks.count >0){
    //init MutableArray and  add three tasks.
    self.tasks = [[NSMutableArray alloc] init];
   
    LDFTask *task1 = [[LDFTask alloc] init];
    task1.toDo = @"Mjölk och falukorv";
    task1.taskHead = @"Handla";
    task1.prio = 1;
    [self.tasks addObject:task1];
   LDFTask *task2 = [[LDFTask alloc] init];
    task2.toDo = @"Tanka 98 på Statoil";
    task2.taskHead = @"Tanka";
    task2.prio = 2;
    [self.tasks addObject:task2];
   LDFTask *task3 = [[LDFTask alloc] init];
    task3.toDo = @"Måste tänka ut en superbra LIA plats";
    task3.taskHead = @"LIA";
    task3.prio = 4;
    [self.tasks addObject:task3];
    }
   
   }



-(void)saveArray{
   // NSLog(@"i saveArray");

    NSData *encodedTasks = [NSKeyedArchiver archivedDataWithRootObject:self.tasks];
    [[NSUserDefaults standardUserDefaults] setObject:encodedTasks forKey:[NSString stringWithFormat:@"tasks"]];
}

-(void)loadArray{
 //NSLog(@"i loadArray");
    
    NSData *decodedTasks = [[NSUserDefaults standardUserDefaults] objectForKey: [NSString stringWithFormat:@"tasks"]];
    NSArray *decodedArray = [NSKeyedUnarchiver unarchiveObjectWithData: decodedTasks];
    self.tasks = [NSMutableArray arrayWithArray:decodedArray];
 //NSLog([self.tasks[1] toDo]);

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.currentRow = indexPath.row;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"taskCell" forIndexPath:indexPath];
    LDFTask *task = [self.tasks objectAtIndex:indexPath.row];
    cell.textLabel.text = task.taskHead;
    cell.detailTextLabel.text = task.toDo;
    self.taskPriority = task.prio;
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png", self.taskPriority]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tasks removeObjectAtIndex:indexPath.row];
    [tableView reloadData];
    [self saveArray];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"details"]){
        LDFTaskDetailsView *taskDetailedView = [ segue destinationViewController ];
        UITableViewCell *cell = sender;
        taskDetailedView.title = cell.textLabel.text;
        taskDetailedView.head = cell.textLabel.text;
        taskDetailedView.info = cell.detailTextLabel.text;
        taskDetailedView.prio = self.taskPriority;
     
    } else if([segue.identifier isEqualToString:@"addTask"]){
        LDFAddTaskView *addView = [ segue destinationViewController ];
        addView.tasks = self.tasks;
        
    }
    
}


@end
