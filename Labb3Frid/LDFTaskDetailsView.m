//
//  LDFTaskDetailsView.m
//  Labb3Frid
//
//  Created by IT-Högskolan on 02/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LDFTaskDetailsView.h"
#import "LDFTaskMan.h"

@interface LDFTaskDetailsView ()
@property (nonatomic) LDFTaskMan *manager;
@end

@implementation LDFTaskDetailsView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.taskInfo.text = self.info;
    self.taskHeader.text = self.head;
    switch (self.prio) {
        case 1:
            self.taskPrio.text = @"No Worries";
            break;
        case 2:
            self.taskPrio.text = @"Don't forget";
            break;
        case 3:
            self.taskPrio.text = @"This IS important";
            break;
        case 4:
            self.taskPrio.text = @"Do it FFS!!";
            break;
        default:
            self.taskPrio.text = @"No priority set";
            break;
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
